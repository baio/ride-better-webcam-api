`gcloud compute firewall-rules create allow-http --description "Incoming 8000 welcomed." --allow tcp:8000 --project ride-better`

##Config

### Set broadcast interval

Allow to config interval for broadcasting image of the device (one spot at time).
`PUT /configs/{spot}/interval/{interval}`

+ spot - spot id
+ interval - interval to broadcast image for the spot

### Reset configs

`DELETE /configs`