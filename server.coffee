hapi = require "hapi"
Promise = require "bluebird"
moment = require "moment"
azure = require "azure-storage"
mongojs = require "mongojs"
log = require "./log"
fs = Promise.promisifyAll require "fs"
joi = require "joi"

#AZURE_STORAGE_ACCOUNT
#AZURE_STORAGE_ACCESS_KEY
#LOGGLY_KEY
#MONGO_URI

db = mongojs(process.env.MONGO_URI, ["webcams"])
webcamsCollection = Promise.promisifyAll db.webcams

port = Number process.env.PORT | "8007"

blob = Promise.promisifyAll azure.createBlobService()

_config = undefined

postSnapshot = (req, resp) ->
  path =  req.payload.path
  spot = req.params.spot
  index = parseInt req.params.index
  key = "rb-" + spot + "-" + index + "-" + moment.utc().format("DD-MM-YY-HH-mm-SS")  + ".jpg"
  log.write oper : "post_request", status : "received", params : {path : path, spot : spot, key : key, index : index}
  blob.createContainerIfNotExistsAsync("ride-better-webcams", publicAccessLevel : 'blob')
  .then ->
      blob.createBlockBlobFromLocalFileAsync("ride-better-webcams", key, path)
  .then ->
      doc =
        spot : spot
        created : moment.utc().toDate()
        key : key
        index : index
      webcamsCollection.insertAsync doc
  .then ->
      log.write oper : "post_request", status : "success", key : key
  .catch (err) ->
      log.write oper : "post_request", status : "error", err : err, key : key
  .finally ->
      fs.unlinkAsync path
  config = if _config and _config.spot == spot then _config
  resp ok : true, config : config

postRoute =
  method : "POST"
  path : "/snapshots/{spot}/{index}"
  config :
    handler : postSnapshot
    payload:
      maxBytes: 209715200
      output: 'file'
      parse: true
      uploads : "./tmp"


postInterval =
  method : "POST"
  path : "/spots/{spot}/config/interval/{interval}"
  config :
    validate :
      params :
        spot : joi.string().required()
        interval : joi.number().required()
  handler : (req, resp) ->
    _config = {spot : req.params.spot, interval : req.params.interval}
    log.write oper : "set_config", status : "success", params :  config : _config
    resp ok : true, config : _config

resetConfig =
  method : "DELETE"
  path : "/config"
  handler : (req, resp) ->
    _config = undefined
    log.write oper : "reset_config", status : "success"
    resp ok : true, config : _config

server = hapi.createServer port
server.route [postRoute, postInterval, resetConfig]

server.start ->
  log.write oper : "server_start", status : "success", params :  port : port
